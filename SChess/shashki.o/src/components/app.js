import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Board from './board';

export default class App extends Component {
	static propTypes = {
		gameName: PropTypes.string.isRequired,
	}

	render() {
		return (
			<div className="app">
				{
					(this.props.gameName!==undefined)?
						<h1>{this.props.gameName}</h1>:
						''
				}
				<Board 
					isBorder={true}
					bordersColor="#000"
					labels={{top:'a',right:1,bottom:'a',left:1}}
					white="#FFF"
					black="#000" />
			</div>
		);
	}
}