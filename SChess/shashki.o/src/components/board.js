import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Square from './square';

export default class Board extends Component {
	static propTypes = {
		white: PropTypes.string,
		black: PropTypes.string,
		isBorder: PropTypes.bool.isRequired,
		labels: PropTypes.oneOfType([
			PropTypes.shape(
				{
					top: PropTypes.oneOfType([
						PropTypes.string,
						PropTypes.number,
					]),
					right: PropTypes.oneOfType([
						PropTypes.string,
						PropTypes.number,
					]),
					bottom: PropTypes.oneOfType([
						PropTypes.string,
						PropTypes.number,
					]),
					left: PropTypes.oneOfType([
						PropTypes.string,
						PropTypes.number,
					]),
				}
			),
			PropTypes.bool
		]).isRequired,
	}

	constructor(props) {
		super(props);

		this.getLabels = this.getLabels.bind(this);

		let row0 = [];
		let row7 = [];
		let column0 = [];
		let column7 = [];

		let labels = this.props.labels;
		if( labels !== false ) {
			row0 = this.getLabels(labels.top);
			row7 = this.getLabels(labels.bottom);
			column0 = this.getLabels(labels.left);
			column7 = this.getLabels(labels.right);
		}

		this.state = {
			battle: [
				[0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0],
				[0,0,0,0,0,0,0,0]
			],
			white: (this.props.white!==undefined)? this.props.white : "#FFF",
			black: (this.props.black!==undefined)? this.props.black : "#000",
			style: {
				border: (this.props.isBorder===true)? "1px solid "+this.props.bordersColor : "none",
			},
			labels: {
				row0: row0,
				row7: row7,
				column0: column0,
				column7: column7
			}
		};

	}

	getLabels(flag) {
		switch(flag) {
			case 1: return [1,2,3,4,5,6,7,8];
			case -1: return [8,7,6,5,4,3,2,1];
			case 'a': return ['a','b','c','d','e','f','g','h'];
			case 'z': return ['h','g','f','e','d','c','b','a'];
			default: return [1,2,3,4,5,6,7,8];
		}
	}

	render() {
		console.log(this.state);
		return (
			<div 
				style={this.state.style}
				className="board" >
				{
					this.state.battle.map((row, i)=>{
						return (
							<div className="row" key={i}>
								{
									row.map((cell, j)=>{
										return (
											<Square 
												key={j}
												row={i}
												column={j}
												labels={this.state.labels}
												fill={((i+j)%2===0)?this.state.white:this.state.black}
												content={this.state.battle[i][j]} />
										);
									})
								}
							</div>
						);
					})
				}
			</div>
		);
	}
}