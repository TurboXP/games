import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Square extends Component {

	static propTypes = {
		row: PropTypes.number.isRequired,
		fill: PropTypes.string.isRequired,
		column: PropTypes.number.isRequired,
		labels: PropTypes.object.isRequired,
	}
	
	constructor(props) {
		super(props);
		let i = this.props.row;
		let j = this.props.column;
		let before = '';
		let after = '';

		if( i === 0 ) before = this.props.labels.row0[j];
		if( j === 0 ) after = this.props.labels.column0[i];
		if( i === 7 ) before = this.props.labels.row7[j];
		if( j === 7 ) after = this.props.labels.column7[i];

		this.state = {
			className: "square row"+i+" column"+j,
			style: {
				background: this.props.fill
			},
			before: before,
			after: after
		}
	}

	render() {
		return (
			<div 
				data-content-before={this.state.before}
				data-content-after={this.state.after}
				style={this.state.style}
				className={this.state.className}>
				{this.props.content}
			</div>
		);
	}
}